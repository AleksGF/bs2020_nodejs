class UserCheckMethods {
  firstName(name) {
    return !!name && name.trim().length;
  }

  lastName(name) {
    return !!name && name.trim().length;
  }

  email(mail) {
    return !!mail && mail.trim().length && (/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/i).test(mail);
  }

  phoneNumber(number) {
    return !!number && number.trim().length && (/^\+380\d{9}$/).test(number);
  }

  password(pwd) {
    return (pwd.length >= 3);
  }
}

const userCheckMethods = new UserCheckMethods();

const userCheck = (userItem, user, isCreating) => {
  let isValid = true;

  if (isCreating) {
    for (let key of Object.keys(user)) {
      if (key === 'id') continue;
      if (!userItem.hasOwnProperty(key)) {
        isValid = false;
      }
    };
  }

  for (let key of Object.keys(userItem)) {
    if (key === 'id') {
      isValid = false;
    }
    if (!user.hasOwnProperty(key)) {
      isValid = false;
    }
  }

  for (let key of Object.keys(userItem)) {   
    if (!userCheckMethods[key](userItem[key])) {
      isValid = false;
    }
  };
  
  return isValid;
}

exports.userCheck = userCheck;
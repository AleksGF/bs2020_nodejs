class FighterCheckMethods {
  name(name) {
    return !!name && name.trim().length;
  }

  health(health) {
    return true;
  }

  power(power) {
    return (Number.isInteger(power) && power > 0 && power <= 100);
  }

  defense(defense) {
    return (Number.isInteger(defense) && defense > 0 && defense <= 10);
  }
}

const fighterCheckMethods = new FighterCheckMethods();

const fighterCheck = (fighterItem, fighter, isCreating) => {
  let isValid = true;

  if (isCreating) {
    for (let key of Object.keys(fighter)) {
      if (key === 'id' || key === 'health') continue;
      if (!fighterItem.hasOwnProperty(key)) {
        isValid = false;
      }
    };
  }

  for (let key of Object.keys(fighterItem)) {
    if (key === 'id') {
      isValid = false;
    }
    if (!fighter.hasOwnProperty(key)) {
      isValid = false;
    }
  }

  for (let key of Object.keys(fighterItem)) {   
    if (!fighterCheckMethods[key](fighterItem[key])) {
      isValid = false;
    }
  };
  
  return isValid;
}

exports.fighterCheck = fighterCheck;
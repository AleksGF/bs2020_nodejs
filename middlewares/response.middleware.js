const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.data) {
      res.status(200).send(res.data);
    } else {
      let errMsg = (res.err && res.err.message) ? res.err.message : 'Error happend. Try later.';
      res.status(400).send({
        error: true,
        message: errMsg
      });
    }
    
    next();   
}

exports.responseMiddleware = responseMiddleware;
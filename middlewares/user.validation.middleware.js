const { makeUserItemFromReq } = require('../services/makeUserItemFromReq');
const { userCheck } = require('./userCheck');
const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const userItem = makeUserItemFromReq(req.body);
  const isValid = userCheck(userItem, user, true);

  if (isValid) {
    next();  
  } else {
    res.status(400).send({ 
      error: true,
      message: "User entity to create isn't valid"
    });
  }  
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
  const userItem = makeUserItemFromReq(req.body);
  const isValid = userCheck(userItem, user, false);

  if (isValid) {
    next();  
  } else {
    res.status(400).send({ 
      error: true,
      message: "User entity to create isn't valid"
    });  
  }   
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
const { makeFighterItemFromReq } = require('../services/makeFighterItemFromReq');
const { fighterCheck } = require('./fighterCheck');
const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
  const fighterItem = req.body;
  const isValid = fighterCheck(fighterItem, fighter, true);

  if (isValid) {
    next();  
  } else {
    res.status(400).send({ 
      error: true,
      message: "Fighter entity to create isn't valid"
    });
  }  
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
  const fighterItem = req.body;
  const isValid = fighterCheck(fighterItem, fighter, false);

  if (isValid) {
    next();  
  } else {
    res.status(400).send({ 
      error: true,
      message: "Fighter entity to create isn't valid"
    });  
  }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router
  .get('/', (req, res, next) => {
    try {
        res.data = UserService.getUsers();
        
        if (res.data === null) {
          res.status(400).send({
            error: true,
            message: "Can't get Users"
          });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .get('/:id', (req, res, next) => {
    try {
      res.data = UserService.search({ id: req.params.id });

      if (res.data === null) {
        throw Error(`Can't find user with id: ${req.params.id}`);
      }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .post('/', createUserValid, (req, res, next) => {
    try {
      if (UserService.search({ firstName: req.body.firstName })) {
        throw Error(`There is a user with name: ${req.body.firstName}`);
      } else if (UserService.search({ lastName: req.body.lastName })) {
        throw Error(`There is a user with name: ${req.body.lastName}`);
      } else if (UserService.search({ email: req.body.email })) {
        throw Error(`There is a user with email: ${req.body.email}`);
      } else if (UserService.search({ phoneNumber: req.body.phoneNumber })) {
        throw Error(`There is a user with Phone Number: ${req.body.phoneNumber}`);
      } else {
        res.data = UserService.postUser(req.body);   
      }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .put('/:id', updateUserValid, (req, res, next) => {
    try {
      if (UserService.search({ id: req.params.id }) === null) {
        throw Error(`Can't find user with id: ${req.params.id}`);
      } else if (UserService.search({ firstName: req.body.firstName })) {
        throw Error(`There is a user with name: ${req.body.firstName}`);
      } else if (UserService.search({ lastName: req.body.lastName })) {
        throw Error(`There is a user with name: ${req.body.lastName}`);
      } else if (UserService.search({ email: req.body.email })) {
        throw Error(`There is a user with email: ${req.body.email}`);
      } else if (UserService.search({ phoneNumber: req.body.phoneNumber })) {
        throw Error(`There is a user with Phone Number: ${req.body.phoneNumber}`);
      } else {
        res.data = UserService.putUser(req.params.id, req.body);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .delete('/:id', (req, res, next) => {
    try {
      if (UserService.search({ id: req.params.id }) === null) {
        res.status(404).send({
          error: true,
          message: `Can't find user with id: ${req.params.id}`
        });
      } else {
        res.data = UserService.deleteUser(req.params.id);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;
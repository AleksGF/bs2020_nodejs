const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router
  .get('/', (req, res, next) => {
    try {
        res.data = FightService.getFights();
        
        if (res.data === null) {
          res.status(400).send({
            error: true,
            message: "Can't get Fights"
          });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .get('/:id', (req, res, next) => {
    try {
      res.data = FightService.search({ id: req.params.id });

      if (res.data === null) {
        throw Error(`Can't find fight with id: ${req.params.id}`);
      }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .post('/', (req, res, next) => {
    try {
      res.data = FightService.postFight(req.body);      
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .delete('/:id', (req, res, next) => {
    try {
      if (FightService.search({ id: req.params.id }) === null) {
        throw Error(`Can't find fight with id: ${req.params.id}`);
      } else {
        res.data = FightService.deleteFight(req.params.id);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);

module.exports = router;
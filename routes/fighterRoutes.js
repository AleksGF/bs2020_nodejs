const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router
  .get('/', (req, res, next) => {
    try {
        res.data = FighterService.getFighters();
        
        if (res.data === null) {
          res.status(400).send({
            error: true,
            message: "Can't get Fighters"
          });
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .get('/:id', (req, res, next) => {
    try {
      res.data = FighterService.search({ id: req.params.id });

      if (res.data === null) {
        throw Error(`Can't find fighter with id: ${req.params.id}`);
      }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .post('/', createFighterValid, (req, res, next) => {
    try {
      if (FighterService.search({ name: req.body.name })) {
        throw Error('There is fighter with this name');
      } else {
        res.data = FighterService.postFighter(req.body); 
      }     
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
  }, responseMiddleware)
  .put('/:id', updateFighterValid, (req, res, next) => {
    try {
      if (FighterService.search({ id: req.params.id }) === null) {
        throw Error(`Can't find fighter with id: ${req.params.id}`);
      } else if (FighterService.search({ name: req.body.name })) {
        throw Error('There is fighter with this name');
      } else {
        res.data = FighterService.putFighter(req.params.id, req.body);
      }   
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware)
  .delete('/:id', (req, res, next) => {
    try {
      if (FighterService.search({ id: req.params.id }) === null) {
        res.status(404).send({
          error: true,
          message: `Can't find fighter with id: ${req.params.id}`
        });
      } else {
        res.data = FighterService.deleteFighter(req.params.id);
      }
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  }, responseMiddleware);
      
module.exports = router;
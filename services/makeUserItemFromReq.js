const { user } = require('../models/user');

const makeUserItemFromReq = (userData) => {
  let item = {};
  
  for (let key of Object.keys(userData)) {
    if (user.hasOwnProperty(key) && key !== 'id') {
      if (key === 'phoneNumber') {
        item[key] = '+' + Array.from(userData[key].matchAll(/\d/g)).join('');
      } else {
        item[key] = userData[key];
      }
    }
  }

  return item;
};

exports.makeUserItemFromReq = makeUserItemFromReq;

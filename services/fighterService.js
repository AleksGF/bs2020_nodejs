const { FighterRepository } = require('../repositories/fighterRepository');
const { makeFighterItemFromReq } = require('./makeFighterItemFromReq');

class FighterService {
    // TODO: Implement methods to work with fighters
  getFighters() {
    const item = FighterRepository.getAll();
    
    if(!item) {
      return null;
    }
    
    return item;
  }

  search(search) {
      const item = FighterRepository.getOne(search);
  
      if(!item) {
          return null;
      }

      return item;
  }

  postFighter(data) {
    const fighterItem = makeFighterItemFromReq(data);
    const item = FighterRepository.create(fighterItem);

    if(!item) {
      return null;
    }

    return item;
  }

  putFighter(id, dataToUpdate) {
    const fighterItem = makeFighterItemFromReq(dataToUpdate);
    const item = FighterRepository.update(id, fighterItem);
    
    if(!item) {
      return null;
    }

    return item;
  }

  deleteFighter(id) {
    const item = FighterRepository.delete(id);
    
    if(!item) {
      return null;
    }

    return item;
  }
}

module.exports = new FighterService();
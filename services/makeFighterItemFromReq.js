const { fighter } = require('../models/fighter');

const makeFighterItemFromReq = (fighterData) => {
  let item = {};
  
  for (let key of Object.keys(fighterData)) {
    if (fighter.hasOwnProperty(key) && key !== 'id' && key !== 'health') {
      item[key] = fighterData[key];
    }
  }

  item.health = 100;

  return item;
};

exports.makeFighterItemFromReq = makeFighterItemFromReq;

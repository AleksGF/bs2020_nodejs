const { UserRepository } = require('../repositories/userRepository');
const { makeUserItemFromReq } = require('./makeUserItemFromReq');

class UserService {
    // TODO: Implement methods to work with user
  getUsers() {
    const item = UserRepository.getAll();
    
    if(!item) {
      return null;
    }
    
    return item;
  }

  search(search) {
      const item = UserRepository.getOne(search);
  
      if(!item) {
          return null;
      }

      return item;
  }

  postUser(data) {
    const userItem = makeUserItemFromReq(data);
    const item = UserRepository.create(userItem);

    if(!item) {
      return null;
    }

    return item;
  }

  putUser(id, dataToUpdate) {
    const userItem = makeUserItemFromReq(dataToUpdate);
    const item = UserRepository.update(id, userItem);
    
    if(!item) {
      return null;
    }

    return item;
  }

  deleteUser(id) {
    const item = UserRepository.delete(id);
    
    if(!item) {
      return null;
    }

    return item;
  }
}

module.exports = new UserService();
const { FightRepository } = require('../repositories/fightRepository');

class FightsService {
    // OPTIONAL TODO: Implement methods to work with fights
 getFights() {
    const item = FightRepository.getAll();
    
    if(!item) {
      return null;
    }
    
    return item;
  }

  search(search) {
      const item = FightRepository.getOne(search);
  
      if(!item) {
          return null;
      }

      return item;
  }

  postFight(data) {
    const item = FightRepository.create(data);

    if(!item) {
      return null;
    }

    return item;
  }

  deleteFight(id) {
    const item = FightRepository.delete(id);
    
    if(!item) {
      return null;
    }

    return item;
  }
}

module.exports = new FightsService();
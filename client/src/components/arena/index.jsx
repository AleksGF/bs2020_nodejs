import React from 'react';
import './index.css';

class Arena extends React.Component {

	render() {        
  	return (
			<div className='arena___root'>
			  <div className='arena___fight-status'>
			    <div className='arena___fighter-indicator'>
			      <span className='arena___fighter-name'>PlayerOne</span>
            <div className='arena___health-indicator'>
              <div className='arena___health-bar' id='leftBar'>
              </div>
			      </div>
			    </div>
			    <div className='arena___fighter-indicator'>
			      <span className='arena___fighter-name'>PlayerTwo</span>
            <div className='arena___health-indicator'>
              <div className='arena___health-bar' id='rightBar'>
              </div>
			      </div>
			    </div>
			  </div>
			  <div className='arena___battlefield'>
          <div className='arena___fighter arena___left-fighter'>
	          <img 
	            className='fighter-preview___img' 
	            src='https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif' 
	            alt='Fighter One' />
          </div>
          <div className='arena___fighter arena___right-fighter'>
	          <img 
	            className='fighter-preview___img' 
	            src='https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif' 
	            alt='Fighter Two' />
          </div>
			  </div>
		  </div>
		);
	}	
}

export default Arena;
